import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AuthService } from '../providers/auth-service';
import { EstadisticasPage } from '../pages/estadisticas/estadisticas';
import { AcercaDePage } from '../pages/acerca-de/acerca-de';
import { DetallesInstitucionesPage } from '../pages/detalles-instituciones/detalles-instituciones';
import { DepartamentosPage } from '../pages/departamentos/departamentos';
import { LoginPage } from "../pages/login/login";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Diagnostic } from '@ionic-native/diagnostic';
import { GooglePlus } from "@ionic-native/google-plus";
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { AngularFireModule } from 'angularfire2';
import {CustomPipe} from "./custom.pipe";
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import {NotificacionesService} from "../providers/notificaciones-service";
import {UserService} from "../providers/user-service";
import {ImportanteService} from "../providers/importante-service";
import {EstadisticasProvider} from "../providers/estadisticas-provider";
import {FilterPage} from "../pages/filter/filter";
import {FilterProvider} from "../providers/filter-provider";
import {UtilsService} from "../providers/utils-service";
import {MapaClientesPage} from "../pages/mapa-clientes/mapa-clientes";
import {MapaClientesService} from "../providers/mapa-clientes-service";
import {PedidosPage} from "../pages/pedidos/pedidos";
import {PedidosPendientesPage} from "../pages/pedidos-pendientes/pedidos-pendientes";
import {PedidosConfirmadosPage} from "../pages/pedidos-confirmados/pedidos-confirmados";
import {PedidosFinalizadosPage} from "../pages/pedidos-finalizados/pedidos-finalizados";
import {PedidosService} from "../providers/pedidos-service";
import {ConcursosService} from "../providers/concursos-service";
import {MapaInstitucionesService} from "../providers/mapa-instituciones-service";
import {FormPedidosPage} from "../pages/form-pedidos/form-pedidos";
import {ProductosService} from "../providers/productos-service";
import {SeleccionarProductosPage} from "../pages/seleccionar-productos/seleccionar-productos";
import {ClientesService} from "../providers/clientes-service";
import {SeleccionarClientePage} from "../pages/seleccionar-cliente/seleccionar-cliente";
import {ProductosPage} from "../pages/productos/productos";
import {ViewPedidosConfirmadosPage} from "../pages/view-pedidos-confirmados/view-pedidos-confirmados";
import {ViewPedidosFinalizadosPage} from "../pages/view-pedidos-finalizados/view-pedidos-finalizados";
import {CalendarioClientesPage} from "../pages/calendario-clientes/calendario-clientes";
import {Calendar} from "@ionic-native/calendar";
import { ListaClientesPage } from '../pages/lista-clientes/lista-clientes';
import { CrearClientePage } from '../pages/crear-cliente/crear-cliente';

/*export const firebaseConfig = {
  apiKey: "AIzaSyASWfAwJHohV2nRQQrv5PKhGUX8IgW7XcQ",
  authDomain: "concursapy-1bdb0.firebaseapp.com",
  databaseURL: "https://concursapy-1bdb0.firebaseio.com",
  projectId: "concursapy-1bdb0",
  storageBucket: "concursapy-1bdb0.appspot.com",
  messagingSenderId: "1080747185323"
};*/
export const firebaseConfig = {
  apiKey: "AIzaSyDm3XvdOwoBlRWCPGz1vyDjrvKZBG0cjqA",
  authDomain: "ventasmultinivel-67bb3.firebaseapp.com",
  databaseURL: "https://ventasmultinivel-67bb3.firebaseio.com",
  projectId: "ventasmultinivel-67bb3",
  storageBucket: "ventasmultinivel-67bb3.appspot.com",
  messagingSenderId: "488619158537"
};

@NgModule({
  declarations: [
    MyApp,
    EstadisticasPage,
    AcercaDePage,
    DepartamentosPage,
    LoginPage,
    CustomPipe,
    FilterPage,
    MapaClientesPage,
    DetallesInstitucionesPage,
    PedidosPage,
    PedidosPendientesPage,
    PedidosConfirmadosPage,
    PedidosFinalizadosPage,
    FormPedidosPage,
    SeleccionarProductosPage,
    SeleccionarClientePage,
    ProductosPage,
    ViewPedidosConfirmadosPage,
    ViewPedidosFinalizadosPage,
    CalendarioClientesPage,
    ViewPedidosFinalizadosPage,
    ListaClientesPage,
    CrearClientePage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    EstadisticasPage,
    AcercaDePage,
    DepartamentosPage,
    LoginPage,
    FilterPage,
    MapaClientesPage,
    DetallesInstitucionesPage,
    PedidosPage,
    PedidosPendientesPage,
    PedidosConfirmadosPage,
    PedidosFinalizadosPage,
    FormPedidosPage,
    SeleccionarProductosPage,
    SeleccionarClientePage,
    ProductosPage,
    ViewPedidosConfirmadosPage,
    ViewPedidosFinalizadosPage,
    CalendarioClientesPage,
    ViewPedidosFinalizadosPage,
    ListaClientesPage,
    CrearClientePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConcursosService,
    MapaInstitucionesService,
    SocialSharing,
    Diagnostic,
    AuthService,
    GooglePlus,
    GoogleAnalytics,
    NotificacionesService,
    UserService,
    ImportanteService,
    EstadisticasProvider,
    FilterProvider,
    FileTransfer,
    File,
    FileOpener,
    UtilsService,
    MapaClientesService,
    PedidosService,
    ProductosService,
    ClientesService,
    Calendar
  ]
})
export class AppModule {}
