import { Component, ViewChild } from '@angular/core';
import {AlertController, Nav, Platform, ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { EstadisticasPage } from '../pages/estadisticas/estadisticas';
import { AcercaDePage } from '../pages/acerca-de/acerca-de';
import {AuthService} from "../providers/auth-service";
import {LoginPage} from "../pages/login/login";
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import {AngularFireAuth} from "angularfire2";
import {PedidosPage} from "../pages/pedidos/pedidos";
import {MapaClientesPage} from "../pages/mapa-clientes/mapa-clientes";
import {ProductosPage} from "../pages/productos/productos";
import {UserService} from "../providers/user-service";
import {CalendarioClientesPage} from "../pages/calendario-clientes/calendario-clientes";
import {ListaClientesPage} from "../pages/lista-clientes/lista-clientes";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //rootPage = ConcursosPage;
  rootPage :any;
  page:boolean = true;
  selectedPage = this.rootPage;
  show:boolean =false;

  pages: Array<{title: string, component: any, icon: string, show: boolean}>;
  pages2: Array<{title: string, component: any, icon: string, show: boolean}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
              public auth$: AngularFireAuth, public auth: AuthService, public alertCtrl: AlertController, public toastCtrl: ToastController,
              private ga: GoogleAnalytics, private userService : UserService) {

    this.initializeApp();

    this.pages = [
      { title: 'Lista de Productos', component: ProductosPage, icon: "pricetags", show:false },
      { title: 'Clientes', component: ListaClientesPage, icon: "notifications", show:true },
    ];

    this.pages2 = [
      { title: 'Mapa de Clientes', component: MapaClientesPage, icon: "map", show:false },
      { title: 'Lista de Pedidos', component: PedidosPage, icon: "list-box", show:false },
      /*{ title: 'Agenda de Clientes', component: CalendarioClientesPage, icon: "md-calendar", show:false },
      { title: 'Estadísticas', component: EstadisticasPage, icon: "stats", show:false },*/
      { title: 'Acerca de Nosotros', component: AcercaDePage, icon: "information-circle", show:false }
    ];

  }
  onDeviceReady() {
  this.splashScreen.hide();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // google
      this.ga.startTrackerWithId("UA-71678136-4")
        .then(() => {
          return this.ga.enableUncaughtExceptionReporting(true)
        }).then((_success) => {
          console.log("startTrackerWithId success")
        }).catch((_error) => {
          console.log("enableUncaughtExceptionReporting", _error);
        });


      this.auth$.subscribe(user => {
        //if(this.page){

        let correoElectronico = this.auth.email();
        //let nombres = this.auth.displayName();
        this.userService.verificarUsuario(correoElectronico)
          .subscribe((payload) => {
            this.rootPage = user ? PedidosPage : LoginPage;
            this.selectedPage = user ? PedidosPage : LoginPage;
          });
          //this.page=false;
        //}
      });
    });
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.rootPage = page.component;
    this.selectedPage = page.component;
  }

  isLoggedIn(): boolean {
    return this.auth.authenticated;
  }

  login() {
    this.nav.push(LoginPage);
  }

  logout() {
    this.auth.signOut();
    this.showToast();
  }

  displayName() {
    return this.auth.displayName();
  }

  displayAvatar(): string {
    if(this.isLoggedIn()){
      return this.auth.displayAvatar();
    }else{
      return "./assets/img/avatar.png";
    }
  }
  email() {
    return this.auth.email();
  }

  confirmLogout() {
    let alert = this.alertCtrl.create({
      title: 'Cerrar Sesión',
      message: 'Está seguro que desea cerrar su sesión?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this.logout();
          }
        }
      ]
    });
    alert.present();
  }

  showToast() {
    let toast = this.toastCtrl.create({
      message: 'Se ha cerrado la sesión',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
    //this.rootPage = LoginPage;

  }

}
