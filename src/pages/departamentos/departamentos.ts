import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import {MapaInstitucionesService} from '../../providers/mapa-instituciones-service';
import {LoadingController} from 'ionic-angular';

/*
  Generated class for the Departamentos page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-departamentos',
  templateUrl: 'departamentos.html'
})
export class DepartamentosPage {
  items : any = null;
  selected: any = 0;
  loader: any;
  hidden : boolean = false;
  nombre: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public viewCtrl: ViewController,public mapaInstitucionesService: MapaInstitucionesService,
              public loadingCtrl: LoadingController) {

    this.selected = {ciudad: "INICIO", id: navParams.get('id') };
    this.items=navParams.get('data');
    //
    // this.loadCiudades(null);
  }

  /*loadCiudades(nombre:any){
    this.mapaInstitucionesService.getCiudades(nombre)
      .subscribe(result => {
        this.items = result;
        this.loader.dismiss();
      });
  }*/

  select(item : any){
    if(item.id != this.selected.id){
      this.selected = item;
      this.viewCtrl.dismiss(this.selected);
    }else{
      this.viewCtrl.dismiss(null);
    }

  }

  dismiss() {
  this.viewCtrl.dismiss();
  }

  /*buscarCiudad(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.loader = this.loadingCtrl.create({
        content: "Cargando...",
      });
      this.loader.present();
      this.nombre = val;
      this.loadCiudades(this.nombre);
    }else{
      this.nombre = null;
    }
  }*/

  loadCiudades(ev: any) {
    this.nombre = ev;
    let loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    if(ev == null ){
      loader.present();
    }
    this.mapaInstitucionesService.getCiudades(this.nombre)
      .subscribe(result => {
        this.items = result;
        loader.dismiss();
      });
  }

  buscarCiudad(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length > 3) {
      this.loadCiudades(val);
    }
  }

  cancelarBusqueda(){
    this.hidden = !this.hidden;
    this.loadCiudades(null);
  }

  limpiarBusqueda(){
    this.loadCiudades(null);
  }
}
