import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams, ViewController} from 'ionic-angular';
import {GoogleAnalytics} from "@ionic-native/google-analytics";
import {UserService} from "../../providers/user-service";
import {ProductosService} from "../../providers/productos-service";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {PedidosService} from "../../providers/pedidos-service";

/*
  Generated class for the Categorias page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-seleccionar-productos',
  templateUrl: 'seleccionar-productos.html'
})
export class SeleccionarProductosPage {

  items : any = null;
  idPedidosClientes : any = null;
  productosSeleccionados : any = null;
  categoria : any = null;
  hidden : boolean = false;
  loading : boolean = true;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController, private domSanitizer: DomSanitizer,
              public productosService: ProductosService, private ga: GoogleAnalytics, public pedidosService : PedidosService,
              private userService: UserService, public viewCtrl: ViewController) {

    this.productosSeleccionados =  navParams.get('productosSeleccionados');
    this.idPedidosClientes =  navParams.get('id');
  }


  ionViewDidEnter() {
    this.getItems(null );
  }

  getItems(ev: any) {
    this.categoria = ev;
    let loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    loader.present();
    let crear = true;
    if(ev != null ){
      crear = false;
    }
    this.pedidosService.getPedidosDetalles(ev, true)
      .subscribe(payload => {
        this.items = payload;
        for(let i in this.items.lista){
          //this.items.lista[i].seleccionado = false;
          let foto : SafeResourceUrl  = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + this.items.lista[i].imagen);
          this.items.lista[i].imageData = foto;
          for(let j in this.productosSeleccionados){
            if(this.items.lista[i].id == this.productosSeleccionados[j].id){
                this.items.lista[i].seleccionado = true;
                this.items.lista[i].cantidad = this.productosSeleccionados[j].cantidad;
                break;
            }
          }
        }

        this.items = this.items.lista;
        loader.dismiss();
        this.loading = false;
      });
  }

  buscarCategorias(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length > 3) {
      this.getItems(val);
    }
  }

  cancelarBusqueda(){
    this.hidden = false;
    this.getItems(null);
  }

  limpiarBusqueda(){
    this.getItems(null);
  }

  guardar() {
    let result = [];
    for(let i in this.items){
        if(this.items[i].seleccionado){
          result.push(this.items[i]);
        }
    }
    this.viewCtrl.dismiss(result);
  }

  close() {
    this.viewCtrl.dismiss(null);
  };

  dismiss() {
    this.viewCtrl.dismiss();
  }

  select(item : any){
    item.seleccionado = !item.seleccionado;
  }
}
