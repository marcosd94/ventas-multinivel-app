import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import {PedidosPendientesPage} from "../pedidos-pendientes/pedidos-pendientes";
import {PedidosConfirmadosPage} from "../pedidos-confirmados/pedidos-confirmados";
import {PedidosFinalizadosPage} from "../pedidos-finalizados/pedidos-finalizados";
import {MapaClientesPage} from "../mapa-clientes/mapa-clientes";

/*
  Generated class for the Concursos page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-pedidos',
  templateUrl: 'pedidos.html'
})
export class PedidosPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = PedidosPendientesPage;
  tab2Root: any = PedidosConfirmadosPage;
  tab3Root: any = PedidosFinalizadosPage;
  tab4Root: any = MapaClientesPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
  }

}
