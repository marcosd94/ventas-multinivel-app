import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import {PedidosService} from "../../providers/pedidos-service";
import {ViewPedidosFinalizadosPage} from "../view-pedidos-finalizados/view-pedidos-finalizados";

/*
  Generated class for the ConcursosFinalizados page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-pedidos-finalizados',
  templateUrl: 'pedidos-finalizados.html'
})
export class PedidosFinalizadosPage {

  items : any = null;
  nombre : string = null;
  hidden : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
              public pedidosService: PedidosService, private ga: GoogleAnalytics) {
  }

  ionViewDidEnter() {
    this.getItems(this.nombre);
  }

  getItems(ev: any) {
    this.nombre = ev;
    let loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    if(ev == null ){
      loader.present();
    }
    this.pedidosService.getPedidosFinalizados(this.nombre, true)
      .subscribe(result => {
        this.items = result;
        this.items = this.items.lista;
        loader.dismiss();
      });
  }

  buscarConcursos(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length > 2) {
      this.getItems(val);
    }
  }

  cancelarBusqueda(){
    this.hidden = false;
    this.getItems(null);
  }

  limpiarBusqueda(){
    this.getItems(null);
  }


  doInfinite(infiniteScroll) {
    this.pedidosService.getPedidosFinalizados(this.nombre, false)
      .subscribe(payload => {
        let lista : any = payload;
        this.items = this.items.concat(lista.lista);
        infiniteScroll.complete();
      });
  }

  goToDetails(item) {
    this.ga.trackEvent("Detalles Concursos Finalizados", item.cargo, item.puesto, 1);
    this.navCtrl.push(ViewPedidosFinalizadosPage, { item: item });
  }

}
