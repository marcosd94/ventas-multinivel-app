import { Component } from '@angular/core';
import {NavController, NavParams, Platform, ToastController} from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import {PedidosService} from "../../providers/pedidos-service";
import {FormPedidosPage} from "../form-pedidos/form-pedidos";

/*
  Generated class for the ConcursosPostulacion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-pedidos-pendientes',
  templateUrl: 'pedidos-pendientes.html'
})
export class PedidosPendientesPage {

  items : any = null;
  nombre : string = null;
  hidden : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
              public pedidosService: PedidosService, private platform: Platform, private ga: GoogleAnalytics, private toastCtrl: ToastController) {

  }

  ionViewDidEnter() {
    this.getItems(this.nombre);
  }

  getItems(ev: any) {
    this.nombre = ev;
    let loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    if(ev == null ){
      loader.present();
    }
    this.pedidosService.getPedidosPendientes(this.nombre, true)
      .subscribe(result => {
        this.items = result;
        this.items = this.items.lista;
        loader.dismiss();
      });
  }

  buscarConcursos(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length > 2) {
      this.getItems(val);
    }
  }

  cancelarBusqueda(){
    this.hidden = false;
    this.getItems(null);
  }

  limpiarBusqueda(){
    this.getItems(null);
  }


  doInfinite(infiniteScroll) {
    this.pedidosService.getPedidosPendientes(this.nombre, false)
      .subscribe(payload => {
        let lista : any = payload;
        this.items = this.items.concat(lista.lista);
        infiniteScroll.complete();
      });
  }

  goToDetails(item) {
    //this.ga.trackEvent("Detalles Concursos en Postulacion", item.cargo, item.puesto, 1);
    this.navCtrl.push(FormPedidosPage, { item: item });
  }


  eliminar(item) {
    this.pedidosService.eliminarPedido(item.id)
      .subscribe(result => {
        let respuesta : any = result;
        console.log(respuesta);
        this.showToast('Pedido eliminado correctamente');
        this.getItems(this.nombre);
      });
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
}
