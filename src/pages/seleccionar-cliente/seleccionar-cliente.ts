import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import {LoadingController} from 'ionic-angular';
import {ClientesService} from "../../providers/clientes-service";
import {UserService} from "../../providers/user-service";

/*
  Generated class for the Departamentos page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-seleccionar-cliente',
  templateUrl: 'seleccionar-cliente.html'
})
export class SeleccionarClientePage {
  items : any = null;
  selected: any = 0;
  loader: any;
  hidden : boolean = false;
  nombre: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public viewCtrl: ViewController,public clientesService: ClientesService,
              public loadingCtrl: LoadingController, private userService: UserService) {

    this.selected = {ciudad: "INICIO", id: navParams.get('id') };
    //this.items=navParams.get('data');
    //
    this.loadClientes(null);
  }

  /*loadCiudades(nombre:any){
    this.mapaInstitucionesService.getCiudades(nombre)
      .subscribe(result => {
        this.items = result;
        this.loader.dismiss();
      });
  }*/

  select(item : any){
    if(item.id != this.selected.id){
      this.selected = item;
      this.viewCtrl.dismiss(this.selected);
    }else{
      this.viewCtrl.dismiss(null);
    }

  }

  dismiss() {
  this.viewCtrl.dismiss();
  }

  /*buscarCiudad(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.loader = this.loadingCtrl.create({
        content: "Cargando...",
      });
      this.loader.present();
      this.nombre = val;
      this.loadCiudades(this.nombre);
    }else{
      this.nombre = null;
    }
  }*/

  loadClientes(ev: any) {
    this.nombre = ev;
    let loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    if(ev == null ){
      loader.present();
    }
    this.clientesService.getListaClientes( this.userService.user.id )
      .subscribe(result => {
        this.items = result;
        this.items = this.items.lista;
        loader.dismiss();
      });
  }

  buscarCiudad(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length > 3) {
      this.loadClientes(val);
    }
  }

  cancelarBusqueda(){
    this.hidden = !this.hidden;
    this.loadClientes(null);
  }

  limpiarBusqueda(){
    this.loadClientes(null);
  }
}
