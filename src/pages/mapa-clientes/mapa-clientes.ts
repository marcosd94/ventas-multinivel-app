import {Component, ViewChild, ElementRef} from '@angular/core';
import {  Geolocation,
  GoogleMapsLatLngBounds
} from 'ionic-native';
import {NavController, NavParams, ModalController, AlertController, LoadingController} from 'ionic-angular';
import {DetallesInstitucionesPage} from '../detalles-instituciones/detalles-instituciones';
import {DepartamentosPage} from '../departamentos/departamentos';
import {LatLng} from "@ionic-native/google-maps";
import {Platform} from 'ionic-angular';
import {Diagnostic} from "@ionic-native/diagnostic";
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import {MapaClientesService} from "../../providers/mapa-clientes-service";
declare var google;
/*
 Generated class for the MapaInstituciones page.


 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-mapa-clientes',
  templateUrl: 'mapa-clientes.html'
})
export class MapaClientesPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  items: any = null;
  loader: any;
  coordenada: any = null;
  hidden : boolean = false;
  nombre : any = null;
  departamento: string;
  idCiudad: any = null;
  status: boolean = true;
  error: boolean = false;
  ciudades: any = null;
  bounds = null;
  markers = [];
  infowindow = [];
  selectedData = {
  descripcionOee:'',
  coordX :null,
  coordY:null,
  descripcionOficina: '',
  nombres:'',
  latitud:null,
  longitud:null};
  showButton : boolean;
  google : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
              public mapaClientesService: MapaClientesService, public modalCtrl: ModalController,
              private alertCtrl: AlertController, private platform: Platform, private diagnostic: Diagnostic,
              private ga: GoogleAnalytics) {
    this.map = null;
    this.showButton = false;
    this.loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    this.loader.present();
    this.checkLocationState();
  }
  ionViewDidEnter() {
    this.showButton = false;
    this.diagnostic.registerLocationStateChangeHandler(this.checkLocationState.bind(this));
  }

  checkLocationState() {
    let locationCallback = (isEnabled) => {
      isEnabled ? this.obtenerPosicion() : this.presentConfirm();
    };
    let requestCallback = (status) => {
      if (status == 'GRANTED') this.diagnostic.isLocationEnabled().then(locationCallback);
    };
    let authorizationCallback = (isAuthorized) => {
      if (isAuthorized) {
        this.diagnostic.isLocationEnabled().then(locationCallback);
      } else {
        this.diagnostic.requestLocationAuthorization(null).then(requestCallback);
      }
    };
    this.diagnostic.isLocationAuthorized().then(authorizationCallback);
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No se pudo obtener la ubicación',
      message: 'Desea activar los servicios de localización?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          this.coordenada = [{
            'longitude': -57.623766,
            'latitude': -25.289342
          }];
          this.initializeMap(this.coordenada);
        }
      },
        {
          text: 'Aceptar',
          handler: () => {
            this.diagnostic.switchToLocationSettings();
          }
        }
      ]
    });
    alert.present();
  }

  obtenerPosicion(): any {
    Geolocation.getCurrentPosition({
      timeout: 10000
    }).then(res => {
      this.coordenada = [{
        'longitude': res.coords.longitude,
        'latitude': res.coords.latitude
      }];
      this.initializeMap(this.coordenada);
    }).catch((error) => {
      this.coordenada = [{
        'longitude': -57.623766,
        'latitude': -25.289342
      }];
      this.initializeMap(this.coordenada);
    });
  }


  initializeMap(coordenada: any[]) {
    let longitude = coordenada[0]['longitude'];
    let latitude = coordenada[0]['latitude'];



    let latLng = new google.maps.LatLng(latitude, longitude);
    let mapOptions = {
      center: latLng,
      zoom: 5,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      fullscreenControl: false
    };

    this.platform.ready().then(() => {

      this.map = new google.maps.Map(document.getElementById("map"), mapOptions);
      this.loadData(null, null, latitude, longitude);
      this.loader.dismiss();
    });
  }


  loadData(departamento, nombre, latitud, longitud) {
    this.disableButton();
    this.mapaClientesService.getClientes(departamento, nombre, latitud, longitud)
      .subscribe(result => {
        this.items = result;
        this.items = this.items.lista;
        this.deleteMarkers();
      });
  }

  addMarkers() {

    this.bounds = new google.maps.LatLngBounds();
    this.infowindow = [];
    let infowindow, marker, i;

    for (i = 0; i < this.items.length; i++) {
/*      let tooltip = '<div id="content">' +
        '<p id="firstHeading" class="firstHeading"><b>' + this.items[i].nombres +' '+ this.items[i].apellidos+ '</b></p>' +
        '<div id="bodyContent">' +
        '<p><b>Email: </b>' + this.items[i].documento + '<br>' +
        /!*'<b>Ciudad: </b>' + this.items[i].ciudad + '<br></p>' +*!/
        '</div>' +
        '</div>';*/


      let tooltip = '<div id="content">' +
        '<p id="firstHeading" class="firstHeading"><b>' + this.items[i].nombres +' '+ this.items[i].apellidos+ '</b></p>' +
        '<div id="bodyContent">'+
        '<p><b>Documento: </b>' + this.items[i].documento + '<br>';
        if(this.items[i].telefono != null && typeof  this.items[i].telefono != 'undefined'){
          tooltip += '<b>Teléfono: </b>' + this.items[i].telefono + '<br>';
        }
        tooltip += '<b>Email: </b>' + this.items[i].email + '<br>' +
          '<b>Fecha Nacimiento: </b>' + this.toDateString(new Date(this.items[i].fechaNacimiento)) + '<br></p>' +
          '</div>' +
          '</div>';
      infowindow = new google.maps.InfoWindow({
        content: tooltip,
        maxWidth: 200,
      });

      let latitud = parseFloat(this.items[i].latitud);
      let longitud = parseFloat(this.items[i].longitud);

      let posicion = new google.maps.LatLng(latitud, longitud);
      this.bounds.extend(posicion);

      marker = new google.maps.Marker({
        position: posicion,
        map: this.map,
        title: i.toString()
      });
      marker.customInfo = this.items[i];

      this.markers.push(marker);
      google.maps.event.addListener(marker, 'click', (function(marker, i, tooltip) {
        return function() {
          infowindow.setContent(tooltip);
          infowindow.open(this.map, marker, tooltip);
          this.enableButton();
          this.selectedData = marker.customInfo;
        }
      })(marker, i, tooltip).bind(this));

      google.maps.event.addListener(infowindow, 'closeclick', (function(marker, i, tooltip) {
        return function() {
          this.disableButton(); //removes the marker
          // then, remove the infowindows name from the array
        }
      })(marker, i, tooltip).bind(this));


    }

    let longitude = this.coordenada[0]['longitude'];
    let latitude = this.coordenada[0]['latitude'];
    let posicionOrigen = new google.maps.LatLng(latitude, longitude);
    this.bounds.extend(posicionOrigen);

    let markerB = new google.maps.Marker({
      position: posicionOrigen,
      map: this.map,
      icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    });
    let ubicacion = "<b>Mi ubicación</b>";
    let infowindow2 = new google.maps.InfoWindow({
      content: ubicacion,
      maxWidth: 200,
    });
    infowindow2.open(this.map, markerB);

    markerB.addListener('click', function() {
      infowindow2.open(this.map, markerB);
    });

    this.markers.push(markerB);

    this.map.fitBounds(this.bounds);
    let zoomChangeBoundsListener =
        google.maps.event.addListenerOnce(this.map, 'bounds_changed', function(event) {
          if (this.getZoom() > 15){
            this.setZoom(15);
          }
        });
    setTimeout(function(){google.maps.event.removeListener(zoomChangeBoundsListener)}, 2000);
    this.loader.dismiss();
  }
  enableButton() {
    document.getElementById("map").style.height = "92%";
    this.showButton = true;
  }

  disableButton() {
    document.getElementById("map").style.height = "100%";
    this.showButton = true;
  }

  goToDetails() {
    if (typeof this.selectedData.descripcionOee != 'undefined' && this.selectedData.descripcionOee != '') {
      this.ga.trackEvent("Detalles de Institución", this.selectedData.descripcionOee, this.selectedData.descripcionOficina, 1);
      this.navCtrl.push(DetallesInstitucionesPage, {
        data: this.selectedData
      });
    } else {
      let alert = this.alertCtrl.create({
        title: 'Notificación',
        subTitle: 'Debe seleccionar un marcador',
        buttons: [{
          text: 'OK',
          role: 'OK',
          handler: () => {}
        }]
      });
      alert.present();
    }
  }


  launchDirections() {
    if (typeof this.selectedData.nombres && this.selectedData.nombres != '') {

      let x = this.selectedData.latitud;
      let y = this.selectedData.longitud;
      window.location.href = "geo:" + x + "," + y + "?q=" + x + "," + y;
    } else {
      let alert = this.alertCtrl.create({
        title: 'Notificación',
        subTitle: 'Debe seleccionar un marcador',
        buttons: [{
          text: 'OK',
          role: 'OK',
          handler: () => {}
        }]
      });
      alert.present();
    }
  }

  busquedaCiudad() {
    if (this.ciudades == null) {
      this.loader = this.loadingCtrl.create({
        content: "Cargando ciudades..."
      });
      this.loader.present();
      this.mapaClientesService.getCiudades(null)
        .subscribe(result => {
          this.ciudades = result;
          this.loader.dismiss();
          this.filtroCiudad();
        });
    } else {
      this.filtroCiudad();
    }
  }
  filtroCiudad() {
    //this.deleteMarkers();
    //this.map.setClickable(false);
    let profileModal = this.modalCtrl.create(DepartamentosPage, {
      id: this.idCiudad,
      ciudad: this.departamento,
      data: this.ciudades
    });

    profileModal.present();
    profileModal.onDidDismiss(data => {
      if (data == 'undefined' || data == null) {
        //this.map.setClickable(true);
      } else {
        this.deleteMarkers();
        this.loader = this.loadingCtrl.create({
          content: "Cargando..."
        });
        this.loader.present();
        this.departamento = data.ciudad;
        this.idCiudad = data.id;
        this.items = [];
        this.loadData(this.departamento, this.nombre, null, null);

        //this.map.setClickable(true);
      }

    });
  }

  deleteMarkers() {
    this.bounds = null;
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
    this.addMarkers();
  }


  moverCamara() {
    let camera = [];
    if (this.items.length > 0) {
      for (let i = 0; i < this.items.length; i++) {
        camera.push(new LatLng(this.items[0].coordX, this.items[0].coordY))
      }

      let latLngBounds = new GoogleMapsLatLngBounds(camera);
      this.map.animateCamera({
        target: latLngBounds,
        zoom: 10
      });
      //this.map.moveCamera(position);
    }
  }

  buscarInstitucion(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.loader = this.loadingCtrl.create({
        content: "Cargando...",
      });
      this.loader.present();
      this.deleteMarkers();
      this.nombre = val;
      this.loadData(this.departamento, val, null, null);
      //this.map.setClickable(true);
      //this.addMarkers();
    } else {
      this.nombre = null;
    }
  }

  toDateString(date) {
    let fecha = date.getFullYear()+'-';
    let month = parseInt(date.getMonth())+1;
    if(month <10){
      fecha+= '0'+month;
    }else{
      fecha+= month;
    }
    let day = date.getDate();

    if(day <10){
      fecha+= '-0'+day;
    }else{
      fecha+= '-'+day;
    }
    return fecha;
  }
}
