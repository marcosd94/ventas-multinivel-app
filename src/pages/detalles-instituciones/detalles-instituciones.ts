import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {Component, ElementRef, ViewChild} from '@angular/core';
import {Geolocation} from 'ionic-native';
import { ConcursosService } from '../../providers/concursos-service';
import {Platform} from 'ionic-angular';
declare var google;

@Component({
  selector: 'page-detalles-instituciones',
  templateUrl: 'detalles-instituciones.html'
})
export class DetallesInstitucionesPage {

  @ViewChild('map') mapElement: ElementRef;
  coordenada: any = null;
  data : any;
  map: any;
  loader: any;
  items : any = null;
  hidden : boolean = true;
  latitude = -25.289342;
  longitud = -57.623766;
  bounds = new google.maps.LatLngBounds();

    constructor(public navCtrl: NavController,public params:NavParams, public loadingCtrl: LoadingController,
                public concursosService: ConcursosService, private platform: Platform) {
      this.map = null;
      this.loader = this.loadingCtrl.create({
        content: "Cargando...",
      });
      this.loader.present();
      this.data = params.get("data");

      this.concursosService.getEnPostulacionInstitucion(this.data.descripcionOee)
        .subscribe(result => {
          this.items = result;
          if(this.items.length<1){
            this.hidden=false;
            this.loader.dismiss();
          }else{
            this.loader.dismiss();
          }
        });
    }

  ionViewDidEnter() {
    this.initializeMap();
    //this.obtenerPosicion();
  }


  obtenerPosicion(): any {

    this.loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    this.loader.present();
    Geolocation.getCurrentPosition({timeout: 10000}).then(res => {
      this.coordenada = [{
        'longitude': res.coords.longitude,
        'latitude': res.coords.latitude
      }];
      this.initializeMap();
    }).catch((error) => {
      //this.error= true;
      this.coordenada = [{
        'longitude': -57.623766,
        'latitude': -25.289342
      }];
    });
  }



  initializeMap() {

    let latitude = parseFloat(this.data.coordX);
    let longitude = parseFloat(this.data.coordY) ;


    let latLng = new google.maps.LatLng(latitude, longitude);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      fullscreenControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.platform.ready().then(() => {

      this.map = new google.maps.Map(document.getElementById("map_det"), mapOptions);
      this.addMarkers();
      this.loader.dismiss();
    });
  }



  addMarkers() {
    /*
    let longitude = this.coordenada[0]['longitude'];
    let latitude = this.coordenada[0]['latitude'];
    let posicionOrigen = new google.maps.LatLng(latitude, longitude);
    this.bounds.extend(posicionOrigen);
    let markerB = new google.maps.Marker({
      position: posicionOrigen,
      map: this.map,
      icon : 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    });

    let ubicacion="<b>Mi ubicación</b>";
    let infowindow2 = new google.maps.InfoWindow({
      content: ubicacion,
      maxWidth: 200
    });
    infowindow2.open(this.map, markerB);

    markerB.addListener('click', function() {
      infowindow2.open(this.map, markerB);
    });*/

    let contentString = '<b>'+this.data.descripcionOee +'</b>';
    let infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 150,
    });

    let latitud = (parseFloat(this.data.coordX)) ? (parseFloat(this.data.coordX)): this.latitude;
    let longitud = (parseFloat(this.data.coordY)) ? (parseFloat(this.data.coordY)) : this.longitud;
    let posicion = new google.maps.LatLng(latitud, longitud);

    let marker = new google.maps.Marker({
      position: posicion,
      map: this.map
    });

    infowindow.open(this.map, marker);
    marker.addListener('click', function() {
      infowindow.open(this.map, marker);
    });

    //this.map.fitBounds(this.bounds, { top: 90, bottom: 40 });
  };

  launchDirections() {
    let x = this.data.coordX || this.latitude;
    let y = this.data.coordY || this.longitud;
    window.location.href = "geo:"+x+","+y+"?q="+x+","+y;
  }
}
