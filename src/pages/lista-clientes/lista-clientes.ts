import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import {ClientesService} from "../../providers/clientes-service";
import {CrearClientePage} from "../crear-cliente/crear-cliente";
import {UserService} from "../../providers/user-service";

/*
  Generated class for the ConcursosEvaluacion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-lista-clientes',
  templateUrl: 'lista-clientes.html'
})
export class ListaClientesPage {

  items : any = null;
  nombre : string = null;
  hidden : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
              public clienteService: ClientesService, private ga: GoogleAnalytics, private userService: UserService) {

  }

  ionViewDidEnter() {
    this.getItems(this.nombre);
  }

  getItems(ev: any) {
    this.nombre = ev;
    let loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    if(ev == null ){
      loader.present();
    }
    this.clienteService.getListaClientes( this.userService.user.id )
      .subscribe(result => {
        this.items = result;
        this.items = this.items.lista;
        loader.dismiss();
      });
  }

  buscarConcursos(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length > 2) {
      this.getItems(val);
    }
  }

  cancelarBusqueda(){
    this.hidden = false;
    this.getItems(null);
  }

  limpiarBusqueda(){
    this.getItems(null);
  }

  doInfinite(infiniteScroll) {
    this.clienteService.getListaClientes( this.userService.user.id )
      .subscribe(payload => {
        let lista : any = payload;
        this.items = this.items.concat(lista.lista);
        infiniteScroll.complete();
      });
  }

  goToDetails(item) {
   // this.ga.trackEvent("Detalles Concursos en Evaluación", item.cargo, item.puesto, 1);
    this.navCtrl.push(CrearClientePage, { item: item });
  }

}
