import {Component, ElementRef, ViewChild} from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {UserService} from "../../providers/user-service";
import {ClientesService} from "../../providers/clientes-service";
import {Diagnostic} from "@ionic-native/diagnostic";
import {Platform} from 'ionic-angular';

import {Geolocation} from 'ionic-native';
declare var google;
/*
 Generated class for the MiCuenta page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-crear-cliente',
  templateUrl: 'crear-cliente.html'
})
export class CrearClientePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  private usuario: FormGroup;
  item: any = null;
  crear : boolean = false;
  loader : any = null;
  coordenada: any = null;
  bounds = null;
  markers = [];
  infowindow = [];
  markerClient : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
              private userService: UserService, private toastCtrl: ToastController, private clienteService: ClientesService,
              private diagnostic: Diagnostic, public loadingCtrl: LoadingController, private alertCtrl: AlertController, private platform: Platform,) {


    this.loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    this.loader.present();
    this.checkLocationState();
    this.usuario = this.formBuilder.group({
      nombres: ['', Validators.required],
      apellidos: ['', Validators.required],
      sexo: [''],
      fechaNacimiento: ['', Validators.required],
      documento: ['', Validators.required],
      telefono: [''],
      email: ['', Validators.required],
    });

    this.item = navParams.get('item');
    if(this.item != null && typeof this.item != 'undefined'){
      this.crear = false;
      this.usuario.patchValue({
        nombres: this.item.nombres,
        apellidos: this.item.apellidos,
        sexo: this.item.sexo,
        fechaNacimiento:  this.toDateString(new Date(this.item.fechaNacimiento)),
        documento: this.item.documento,
        telefono: this.item.telefono,
        email: this.item.email
      });
    }else{
      this.crear = true;
      //this.obtenerDetallesPedidos(true);
    }
  }

  ionViewDidEnter() {
    this.diagnostic.registerLocationStateChangeHandler(this.checkLocationState.bind(this));
  }
  checkLocationState() {
    let locationCallback = (isEnabled) => {
      isEnabled ? this.obtenerPosicion() : this.presentConfirm();
    };
    let requestCallback = (status) => {
      if (status == 'GRANTED') this.diagnostic.isLocationEnabled().then(locationCallback);
    };
    let authorizationCallback = (isAuthorized) => {
      if (isAuthorized) {
        this.diagnostic.isLocationEnabled().then(locationCallback);
      } else {
        this.diagnostic.requestLocationAuthorization(null).then(requestCallback);
      }
    };
    this.diagnostic.isLocationAuthorized().then(authorizationCallback);
  }
  guardar() {
    if(this.usuario.valid){
      if( typeof this.item != 'undefined' && ( typeof this.item.latitud != 'undefined' && this.item.latitud != null ) && ( typeof this.item.longitud != 'undefined' && this.item.longitud != null ) ){
        let value = this.usuario.getRawValue();
        let personas ={};
        let param : any ={};
        if(this.crear){
          personas ={
            "apellidos": value.apellidos,
            "nombres": value.nombres,
            "documento": value.documento,
            "latitud": this.item.latitud,
            "longitud": this.item.longitud,
            "telefono": value.telefono,
            "email": value.email,
            "sexo": value.sexo,
            "fechaNacimiento": value.fechaNacimiento
          };

        }else{
          param.idClientes = this.item.id;
          personas ={
            "id": this.item.idPersonas,
            "apellidos": value.apellidos,
            "nombres": value.nombres,
            "documento": value.documento,
            "latitud": this.item.latitud,
            "longitud": this.item.longitud,
            "telefono": value.telefono,
            "email": value.email,
            "sexo": value.sexo,
            "fechaNacimiento": value.fechaNacimiento
          };
        }
        param.personas = personas;
        param.usuarioActual = this.userService.user.id;

        this.clienteService.insertarCliente(param)
          .subscribe(result => {
            let respuesta : any = result;
            if(respuesta.estado == 'OK'){
              if(this.crear){
                this.showToast('Cliente creado correctamente');
                this.navCtrl.popToRoot();
              }else{
                this.showToast('Datos del cliente modificados correctamente');
                this.navCtrl.popToRoot();
              }
            }else{
              this.showToast(respuesta.mensaje + " - Por favor verifique los datos. ");
            }
          });
      }else{
        this.showToast('Por favor seleccione la ubicación del cliente.');
      }


    }else{
      this.showToast('Complete los campos requeridos por favor');
    }
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  toDateString(date) {
    let fecha = date.getFullYear()+'-';
    let month = parseInt(date.getMonth())+1;
    if(month <10){
      fecha+= '0'+month;
    }else{
      fecha+= month;
    }
    let day = date.getDate();

    if(day <10){
      fecha+= '-0'+day;
    }else{
      fecha+= '-'+day;
    }
    return fecha;
  }


  /*********************** MAPA DE LOCALIZACION **************************/

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No se pudo obtener la ubicación',
      message: 'Desea activar los servicios de localización?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          this.coordenada = [{
            'longitude': -57.623766,
            'latitude': -25.289342
          }];
          this.initializeMap(this.coordenada);
        }
      },
        {
          text: 'Aceptar',
          handler: () => {
            this.diagnostic.switchToLocationSettings();
          }
        }
      ]
    });
    alert.present();
  }

  obtenerPosicion(): any {
    Geolocation.getCurrentPosition({
      timeout: 10000
    }).then(res => {
      this.coordenada = [{
        'longitude': res.coords.longitude,
        'latitude': res.coords.latitude
      }];
      this.initializeMap(this.coordenada);
    }).catch((error) => {
      this.coordenada = [{
        'longitude': -57.623766,
        'latitude': -25.289342
      }];
      this.initializeMap(this.coordenada);
    });
  }


  initializeMap(coordenada: any[]) {
    let longitude = coordenada[0]['longitude'];
    let latitude = coordenada[0]['latitude'];



    let latLng = new google.maps.LatLng(latitude, longitude);
    let mapOptions = {
      center: latLng,
      zoom: 5,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    };

    this.platform.ready().then(() => {

      this.map = new google.maps.Map(document.getElementById("map"), mapOptions);
      this.addMarkers();
      this.loader.dismiss();
    });
  }




  addMarkers() {
    this.bounds = new google.maps.LatLngBounds();
    if(!this.crear && ( typeof this.item.latitud != 'undefined' && this.item.latitud != null ) && ( typeof this.item.longitud != 'undefined' && this.item.longitud != null )){


      let latitud = parseFloat(this.item.latitud);
      let longitud = parseFloat(this.item.longitud);
      let posicion = new google.maps.LatLng(latitud, longitud);
      this.bounds.extend(posicion);

      this.markerClient = new google.maps.Marker({
        position: posicion,
        map: this.map
      });

      let cliente = "<b>Ubicación de "+this.item.nombres+" "+ this.item.apellidos+"</b>";
      let infowindow = new google.maps.InfoWindow({
        content: cliente,
        maxWidth: 200,
      });
      infowindow.open(this.map, this.markerClient);

      this.markerClient.addListener('click', function() {
        infowindow.open(this.map, this.markerClient);
      });

    }


    let longitude = this.coordenada[0]['longitude'];
    let latitude = this.coordenada[0]['latitude'];
    let posicionOrigen = new google.maps.LatLng(latitude, longitude);
    this.bounds.extend(posicionOrigen);

    let markerB = new google.maps.Marker({
      position: posicionOrigen,
      map: this.map,
      icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    });
    let ubicacion = "<b>Mi ubicación</b>";
    let infowindow2 = new google.maps.InfoWindow({
      content: ubicacion,
      maxWidth: 200,
    });
    infowindow2.open(this.map, markerB);

    markerB.addListener('click', function() {
      infowindow2.open(this.map, markerB);
    });

    this.markers.push(markerB);

    this.map.fitBounds(this.bounds);
    let zoomChangeBoundsListener =
      google.maps.event.addListenerOnce(this.map, 'bounds_changed', function(event) {
        if (this.getZoom() > 15){
          this.setZoom(15);
        }
      });
    setTimeout(function(){google.maps.event.removeListener(zoomChangeBoundsListener)}, 2000);

    google.maps.event.addListener(this.map, 'click', (function(event) {
      if(typeof this.markerClient != 'undefined'){
        this.markerClient.setMap(null);
      }
      console.log(event);
      this.bounds.extend(event.latLng);
      if(typeof this.item == 'undefined' || this.item == null){
        this.item = {};
      }
      this.item.latitud = event.latLng.lat();
      this.item.longitud = event.latLng.lng();

      this.markerClient = new google.maps.Marker({
        position: event.latLng,
        map: this.map
      });

      let cliente = "<b>Ubicación del Cliente</b>";
      let infowindow = new google.maps.InfoWindow({
        content: cliente,
        maxWidth: 200,
      });
      infowindow.open(this.map, this.markerClient);

      this.markerClient.addListener('click', function() {
        infowindow.open(this.map, this.markerClient);
      });
    }).bind(this));
    this.loader.dismiss();
  }
}
