import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {FormGroup, FormBuilder} from "@angular/forms";
import {FilterProvider} from "../../providers/filter-provider";

/*
  Generated class for the FilterPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html'
})
export class FilterPage {

  private filter: FormGroup;
  private respuesta: any = {};
  private exist : any;
  categorias : any = null;
  instituciones : any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuilder: FormBuilder, public viewCtrl: ViewController,
              private provider : FilterProvider) {
    this.getCategorias(null);
    this.getInstituciones(null);
    this.exist = navParams.get('data');
    this.filter = this.formBuilder.group({
      categoria: [''],
      institucion: [''],
      vinculo: [''],
      anho: ['']
    });

    if(this.exist != null){

      this.filter.patchValue({
        categoria: this.exist.categoria,
        institucion: this.exist.institucion,
        vinculo: this.exist.vinculo,
        anho: this.exist.anho
      });
    }
  }

  guardar() {
    let value = this.filter.getRawValue();
    this.respuesta.categoria= value.categoria;
    this.respuesta.institucion= value.institucion;
    this.respuesta.vinculo= value.vinculo;
    this.respuesta.anho= value.anho;
    this.viewCtrl.dismiss(this.respuesta);
  }

  close() {
    this.viewCtrl.dismiss(null);
  };

  loadCategorias(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length > 3) {
      this.getCategorias(val);
    }
  }
  loadInstituciones(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length > 3) {
      this.getInstituciones(val);
    }
  }
  getCategorias(ev: any) {

    this.provider.getCategorias(ev)
      .subscribe(result => {
        this.categorias = result;
        //loader.dismiss();
      });
  }
  getInstituciones(ev: any){

    this.provider.getInstituciones(ev)
      .subscribe(result => {
        this.instituciones = result;
        //loader.dismiss();
      });
  }

}
