import { Component } from '@angular/core';
import {NavController, Platform} from "ionic-angular";
import {Calendar} from "@ionic-native/calendar";

@Component({
  selector: 'calendario-clientes',
  templateUrl: 'calendario-clientes.html'
})
export class CalendarioClientesPage  {
  calendars = [];

  constructor(public navCtrl: NavController, private calendar: Calendar, private plt: Platform) {


    this.calendar.hasReadWritePermission().then(

      () =>
        this.calendar.listCalendars().then(data => {
          this.calendars = data;
        }),

      (err) => console.log(err)

    );
  }
  checkCalendarPermision() {
    this.calendar.hasReadWritePermission().then(
      (isEnabled) => {
        isEnabled ? this.createCalendar() : authorizationCallback();}
    );
    let authorizationCallback = () => {
      this.calendar.requestReadWritePermission().then(this.checkCalendarPermision);
    };
  }
  createCalendar() {
    this.calendar.createCalendar('MyCalendar').then(
      (msg) => { console.log(msg); },
      (err) => { console.log(err); }
    );
  }
  addEvent(cal) {
    let date = new Date();
    let options = { calendarId: cal.id, calendarName: cal.name, url: 'https://ionicacademy.com', firstReminderMinutes: 15 };

    this.calendar.createEventInteractivelyWithOptions('My new Event', 'Münster', 'Special Notes', date, date, options).then(res => {
    }, err => {
      console.log('err: ', err);
    });
  }

  openCal(cal) {
    this.navCtrl.push('CalDetailsPage', { name: cal.name })
  }


}
