import {Component} from '@angular/core';
import {AlertController, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {UserService} from "../../providers/user-service";
import {ProductosService} from "../../providers/productos-service";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {SeleccionarProductosPage} from "../seleccionar-productos/seleccionar-productos";
import {SeleccionarClientePage} from "../seleccionar-cliente/seleccionar-cliente";
import {PedidosService} from "../../providers/pedidos-service";

/*
 Generated class for the MiCuenta page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-view-pedidos-confirmados',
  templateUrl: 'view-pedidos-confirmados.html'
})
export class ViewPedidosConfirmadosPage {
  private pedido: FormGroup;
  item: any;
  items:any = null;
  agregado : boolean = false;
  loading: boolean = true;
  cliente : any = null;
  crear : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
              private userService: UserService, private toastCtrl: ToastController, private productosService: ProductosService,
              private domSanitizer: DomSanitizer,  public modalCtrl: ModalController, private pedidosService: PedidosService, public alertCtrl: AlertController) {

    this.pedido = this.formBuilder.group({
      cliente: ['', Validators.required],
      vendedor: ['', Validators.required],
      fechaPedido:  ['', Validators.required]
    });

    this.item = navParams.get('item');
    if(this.item != null && typeof this.item != 'undefined'){
      this.crear = false;
      this.pedido.patchValue({
        cliente: this.item.clienteDescripcion,
        vendedor: userService.user.codigoUsuario,
        fechaPedido: this.toDateString(new Date(this.item.fechaPedido))
      });
      this.obtenerDetallesPedidos(false);
    }else{
      this.crear = true;
      this.loading = false;
      this.pedido.patchValue({
        vendedor: userService.user.codigoUsuario,
        fechaPedido: this.toDateString(new Date())
      });
      //this.obtenerDetallesPedidos(true);
    }
  }

  guardar() {
    if(this.pedido.valid){
      if(this.items != null && this.items.length > 0){
        if(this.crear){
          let pedidosClientes = {
            idClientes:this.cliente.id,
            idUsuarioVendedor:this.userService.user.id,
            fechaPedido: new Date(),
            confirmado:false,
            estadoPedido : 'PEN'
          };
          let pedidosClientesDetalles = [];
          for(let i in this.items){
            let objeto = {
              precioVenta:this.items[i].precioVenta,
              cantidad:this.items[i].cantidad,
              cantidadEntregada:0,
              fechaPedido: new Date(),
              idProductos:this.items[i].id,
              agregado:false
            };
            pedidosClientesDetalles.push(objeto);
          }

          let pedidosClientesParam = {
            pedidosClientes : pedidosClientes,
            pedidosClientesDetalles : pedidosClientesDetalles
          };
          this.pedidosService.insertarPedido(pedidosClientesParam)
            .subscribe(result => {
              if(typeof result.id != 'undefined'){
                this.showToast('Pedido creado correctamente');
                this.navCtrl.popToRoot();
              }else{
                this.showToast('Error al crear el pedido, por favor verifique los datos');
              }
            });

        }else{


          let pedidosClientes = {
            id:this.item.id,
            idClientes:this.item.idClientes,
            idUsuarioVendedor:this.userService.user.id,
            fechaPedido: new Date(),
            confirmado:false,
            estadoPedido : 'PEN'
          };
          let pedidosClientesDetalles = [];
          for(let i in this.items){
            let objeto = {
              //id:this.items[i].idPedidosClientesDetalles,
              idPedidosClientes: this.item.id,
              precioVenta:this.items[i].precioVenta,
              cantidad:this.items[i].cantidad,
              cantidadEntregada:0,
              fechaPedido: this.items[i].cantidad != null ? this.items[i].cantidad : new Date(),
              idProductos:this.items[i].id,
              agregado:false
            };
            pedidosClientesDetalles.push(objeto);
          }

          let pedidosClientesParam = {
            pedidosClientes : pedidosClientes,
            pedidosClientesDetalles : pedidosClientesDetalles
          };

          this.pedidosService.actualizarPedido(pedidosClientesParam)
            .subscribe(result => {
              if(typeof result.id != 'undefined'){
                this.showToast('Pedido actualizado correctamente');
                this.navCtrl.popToRoot();
              }else{
                this.showToast('Error al crear el pedido, por favor verifique los datos');
              }
            });
        }
      }else{
        this.showToast('Debe agregar al menos un detalle al pedido');
      }

    }else{
      this.showToast('Complete los campos requeridos por favor');
    }
  }


  buscarClientes() {
    let profileModal = this.modalCtrl.create(SeleccionarClientePage, {
      id: null
    });

    profileModal.present();
    profileModal.onDidDismiss(data => {
      if (data != 'undefined' && data != null) {
        this.cliente = data;

        this.pedido.patchValue({
          cliente: this.cliente.nombres +' '+this.cliente.apellidos,
          vendedor: this.userService.user.codigoUsuario,
          fechaPedido: this.toDateString(new Date())
        });
      } else {
        this.agregado = false;
      }

    });
  }

  agregarProductos() {
    let profileModal = this.modalCtrl.create(SeleccionarProductosPage, {
      productosSeleccionados: this.items,
      id: typeof this.item != 'undefined' ? this.item.id : null
    });

    profileModal.present();
    profileModal.onDidDismiss(data => {
      if (data != 'undefined' && data != null) {
        if(data.length > 0){
          this.items = data;
          for(let i in this.items){
            let foto : SafeResourceUrl  = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + this.items[i].imagen);
            this.items[i].imageData = foto;
            if(this.items[i].cantidad == 0){
              this.items[i].cantidad = 1
            }
            //this.items[i].cantidadPedida = 1;
          }
          this.agregado = true;
        }else{
          this.agregado = false;
        }
      } else {
        this.agregado = false;
      }

    });
  }


  toDateString(date) {
    let fecha = date.getFullYear()+'-';
    let month = parseInt(date.getMonth())+1;
    if(month <10){
      fecha+= '0'+month;
    }else{
      fecha+= month;
    }
    let day = date.getDate();

    if(day <10){
      fecha+= '-0'+day;
    }else{
      fecha+= '-'+day;
    }
    return fecha;
  }

  obtenerDetallesPedidos(crear){
    let id = null;
    if(!crear){
      id = this.item.id;
    }
    this.pedidosService.getPedidosDetalles(id, crear)
      .subscribe(payload => {
        this.items = payload
        this.items = this.items.lista;
        for(let i in this.items){
          let foto : SafeResourceUrl  = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + this.items[i].imagen);
          this.items[i].imageData = foto;
          //this.items[i].cantidadPedida = 1;
        }
        this.agregado = true;
        this.loading = false;
      });
  }
  restarCantidad(item){
    if(item.cantidad>1){
      item.cantidad--;
    }else{
      this.showToast('La cantidad mínima a solicitar es de: 1 (UNO) ');
    }
  }

  sumarCantidad(item){
    if(item.stockActual > item.cantidad){
      item.cantidad++;
    }else{
      this.showToast('No puede sobrepasar el stock actual!');
    }
  }

  eliminar(item){
    for(let i in this.items){
      if(this.items[i].id == item.id){
        this.items.splice(i , 1);
      }
    }
  }

  confirmarCerrar() {
    let alert = this.alertCtrl.create({
      title: 'Cerrar Pedido',
      message: 'Está seguro que desea cerrar el pedido? Recuerde guardar sus cambios antes de CERRAR!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this.cerrar();
          }
        }
      ]
    });
    alert.present();
  }

  cerrar(){
    this.pedidosService.cerrarPedido(this.item.id)
      .subscribe(result => {
        if(typeof result.id != 'undefined'){
          this.showToast('Pedido cerrado correctamente');
          this.navCtrl.popToRoot();
        }else{
          this.showToast('Error al cerrado el pedido, por favor verifique los datos');
        }
      });
  }
  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
