import {Component} from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {AuthService} from "../../providers/auth-service";
import {UserService} from "../../providers/user-service";

declare var FCMPlugin;

/*
 Generated class for the Login page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public isLogginIn: boolean;
  dataUsuario : any = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService,
              public toastCtrl: ToastController, public userService: UserService) {
  }

  ionViewDidLoad() {
  }



  signInWithGoogle(): void {
    this.isLogginIn = true;
    this.auth.signInWithGoogle()
      .then(() => this.onSignInSuccess())
      .catch((e) => this.onError(e));
  }

  onError(error): void {
    this.isLogginIn = false;
    let errorMessage = error.message;
    this.showToast(errorMessage);
  }

  onSignInSuccess(): void {
    //let idFirebase = firebase.auth().currentUser.uid;
    let correoElectronico = this.auth.email();
    //let nombres = this.auth.displayName();
    this.userService.verificarUsuario(correoElectronico)
      .subscribe((payload) => {
      this.dataUsuario = payload;
      if(this.dataUsuario != null  && typeof this.dataUsuario != 'undefined' ){
        this.showToast("Bienvenido: "+this.dataUsuario.nombres +' '+this.dataUsuario.apellidos);
        //this.navCtrl.push(ConcursosPage);
      }else{
        this.isLogginIn = false;
        this.showToast("No existe el usuario para el correo: "+this.auth.email());
      }
      });
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
