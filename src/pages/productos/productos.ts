import {Component, ElementRef, ViewChild} from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import {ProductosService} from "../../providers/productos-service";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";

/*
  Generated class for the ConcursosPostulacion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-productos',
  templateUrl: 'productos.html'
})
export class ProductosPage {

  @ViewChild('imgCrearPersonas') mapElement: ElementRef;
  items : any = null;
  nombre : string = null;
  hidden : boolean = false;
  data : any = null;
  mostrar : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
              public productosService: ProductosService, private platform: Platform, private domSanitizer: DomSanitizer) {
  }
  ionViewDidEnter() {
    this.getItems(this.nombre);
  }

  getItems(ev: any) {
    this.nombre = ev;
    let loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    if(ev == null ){
      loader.present();
    }
    this.productosService.getProductos()
      .subscribe(result => {
        this.data = result;
        this.items = this.data.lista;
        for(let i in this.items){
          let foto : SafeResourceUrl  = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + this.items[i].imagen);
          this.items[i].imageData = foto;
        }

        loader.dismiss();
      });
  }

  buscarConcursos(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length > 2) {
      this.getItems(val);
    }
  }

  cancelarBusqueda(){
    this.hidden = false;
    this.getItems(null);
  }

  limpiarBusqueda(){
    this.getItems(null);
  }


  doInfinite(infiniteScroll) {
    this.productosService.getProductos()
      .subscribe(payload => {
        this.items = this.items.concat(payload);
        infiniteScroll.complete();
      });
  }

  goToDetails(item) {
    //this.navCtrl.push(ConcursosPostulacionDetallePage, { item: item });
  }

}
