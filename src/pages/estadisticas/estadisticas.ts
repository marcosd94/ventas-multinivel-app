import { Component, ViewChild  } from '@angular/core';
import {NavController, NavParams, PopoverController, LoadingController, ModalController} from 'ionic-angular';
import { Chart } from 'chart.js';
import {EstadisticasProvider} from "../../providers/estadisticas-provider";
import {FilterPage} from "../filter/filter";
/*
 Generated class for the Estadisticas page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-estadisticas',
  templateUrl: 'estadisticas.html'
})
export class EstadisticasPage {


  @ViewChild('barCanvas') barCanvas;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  @ViewChild('pcdCanvas') pcdCanvas;

  barChart: any;
  doughnutChart: any;
  pcd:any;

  postulantesCategorias: any = null;
  categorias = ['SIN DATOS'];
  cantidadVacia= [0];
  postulantes= [];
  vacancias= [];

  suscripciones: any = null;
  categoriasSuscriptas = ['SIN DATOS'];
  cantidadSuscripciones = [1];
  totalSuscripciones = 1;
  filter:any = {};
  show = true;
  postulantesFlag = false;
  suscripcionesFlag = false;
  loader : any;

  /*PCD*/
  tipo = ['SIN DATOS'];
  cantidadConcursos= [0];
  pcdData:any = null;
  totalConcursos= 0;
  totalGeneralConcurso=0;
  today: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private estadisticasProvider: EstadisticasProvider,
              public popoverCtrl: PopoverController, public loadingCtrl: LoadingController, public modalCtrl: ModalController) {

    this.today = new Date();
    this.filter.anho =  this.today.getFullYear();

    this.loader = this.loadingCtrl.create({
      content: "Cargando...",
    });

    this.loader.present();

    this.loadData(null, null, null);
  }

  ionViewDidLoad() {
  }

  loadData(categoria, institucion, vinculo) {
    this.estadisticasProvider.getPostulantes(null, null, null, 2017)
      .subscribe(result => {
        this.postulantesCategorias = result;
        //loader.dismiss();
        if(this.postulantesCategorias.categoria.length > 0){
          this.show = false;
          this.categorias = this.postulantesCategorias.categoria;
        }
        this.postulantes = this.postulantesCategorias.postulantes;
        this.vacancias = this.postulantesCategorias.vacancias;
        this.showPostulantes();
        this.postulantesFlag = true;
        if(this.postulantesFlag && this.suscripcionesFlag){
          this.loader.dismiss();
        }
      });
    this.estadisticasProvider.getSuscripciones(categoria, null, 2017)
      .subscribe(result => {
        this.suscripciones = result;
        //loader.dismiss();
        if(this.suscripciones.categorias.length>0){
          this.categoriasSuscriptas = this.suscripciones.categorias;
          this.cantidadSuscripciones = this.suscripciones.suscriptos;
          this.totalSuscripciones = this.suscripciones.cantidad;
        }

        this.suscripcionesFlag = true;
        if(this.postulantesFlag && this.suscripcionesFlag){
          this.loader.dismiss();
        }
        this.showSuscripciones();
      });

    this.estadisticasProvider.getConcursosPcd(null, null, null, 2017)
      .subscribe(result => {
        this.pcdData = result;
        this.tipo = this.pcdData.tipo;
        this.cantidadConcursos = this.pcdData.cantidad;
        this.showConcursosPcd();
        this.postulantesFlag = true;
        for(let i in this.cantidadConcursos){
          this.totalConcursos += this.cantidadConcursos[i];
          this.totalGeneralConcurso += this.cantidadConcursos[i];
        }
        if(this.postulantesFlag && this.suscripcionesFlag){
          this.loader.dismiss();
        }
      });
  }

  reloadData(categoria, institucion, vinculo, anho) {

    this.postulantesFlag = false;
    this.suscripcionesFlag = false;
    this.loader = this.loadingCtrl.create({
      content: "Cargando...",
    });
    this.loader.present();

    this.estadisticasProvider.getPostulantes(categoria, institucion, vinculo, anho)
      .subscribe(result => {
        this.postulantesCategorias = result;
        //loader.dismiss();

        this.categorias = this.postulantesCategorias.categoria;
        this.postulantes = this.postulantesCategorias.postulantes;
        this.vacancias = this.postulantesCategorias.vacancias;
        this.barChart.options.scales.xAxes[0].display = false;
        if(this.postulantesCategorias.categoria.length <= 0){
          this.barChart.options.scales.xAxes[0].display = true;
          this.categorias =  ['SIN DATOS'];
        }

        this.barChart.options.title.text = this.filter.anho;

        this.barChart.update();
        this.postulantesFlag = true;
        if(this.postulantesFlag && this.suscripcionesFlag){
          this.loader.dismiss();
        }
        this.addDataBar(this.barChart, this.categorias, this.vacancias, this.postulantes);
        //this.addData(this.barChart, label, data);
        //this.barChart.update();
      });
    this.estadisticasProvider.getSuscripciones(categoria, null, anho)
      .subscribe(result => {
        this.suscripciones = result;
        //loader.dismiss();

        this.categoriasSuscriptas = this.suscripciones.categorias;
        this.cantidadSuscripciones = this.suscripciones.suscriptos;
        this.totalSuscripciones = this.suscripciones.cantidad;

        if(this.suscripciones.categorias.length >= 5){
          this.doughnutChart.options.legend.display = false;
        }else{
          this.doughnutChart.options.legend.display = true;
        }

        this.doughnutChart.options.title.text = this.filter.anho;

        if(this.suscripciones.categorias.length <= 0 ){
          this.categoriasSuscriptas = ['SIN DATOS'];
          this.cantidadSuscripciones =  [1];
          this.totalSuscripciones = 1;
          this.doughnutChart.options.legend.display = true;
        }

        this.suscripcionesFlag = true;
        if(this.postulantesFlag && this.suscripcionesFlag){
          this.loader.dismiss();
        }
        this.addData(this.doughnutChart, this.categoriasSuscriptas, this.cantidadSuscripciones);
        //this.doughnutChart.update();
      });


    this.estadisticasProvider.getConcursosPcd(categoria, institucion, vinculo, anho)
      .subscribe(result => {

        this.pcdData = result;
        this.tipo = this.pcdData.tipo;
        this.cantidadConcursos = this.pcdData.cantidad;

        this.totalConcursos = 0;
        this.totalGeneralConcurso = 0;
        for(let i in this.cantidadConcursos){
          this.totalConcursos += this.cantidadConcursos[i];
          this.totalGeneralConcurso += this.cantidadConcursos[i];
        }

        this.pcd.options.title.text = this.filter.anho;

        if(this.cantidadConcursos.length <= 0 ){
          this.tipo = ['SIN DATOS'];
          this.cantidadConcursos =  [1];
          this.totalConcursos = 1;
          this.totalGeneralConcurso = 0;
        }
        this.addData(this.pcd, this.tipo, this.cantidadConcursos);
        //this.doughnutChart.update();
      });
  }

  showPostulantes(){
    this.barChart = new Chart(this.barCanvas.nativeElement, {

      type: 'bar',
      data: {
        labels: this.categorias,
        datasets: [{
          label: 'Vacancias',
          backgroundColor: "#ffa4ba",
          borderColor: "#ff3c5f",
          borderWidth: 1.5,
          data: this.vacancias
        }, {
          label: 'Postulantes',
          data: this.postulantes,
          backgroundColor: "#ffe77f",
          borderColor: "#ffc71b",
          borderWidth: 1.5
        }]

      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
          display: true,
          text: this.filter.anho
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
            ,
            scaleLabel:{
              display: true,
              labelString: 'Cantidad',
              fontColor: "#546372"
            }
          }
          ],
          xAxes: [{
            ticks: {
              display: false
            },
            scaleLabel:{
              display: true,
              labelString: 'Categorías',
              fontColor: "#546372"
            }
          }]
        }
      }

    });
    this.barChart.scales["x-axis-0"].hidden = true;
    this.barChart.update();
  }

  showSuscripciones(){
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        labels: this.categoriasSuscriptas,
        datasets: [{
          label: this.totalSuscripciones+ ' suscripciones',
          data: this.cantidadSuscripciones,
          backgroundColor: [
            "#f99088",
            "#8e99d4",
            "#ffff69",
            "#79a17c",
            "#c580d1",
            "#77ffff",
            "#ffbd69",
            "#b09b94",
            "#88d5c6",
            "#78d588",
            "#ff8fb5",
            "#fff1b4",
            "#ffc18b",
            "#f892ff",
            "#71ffd3",
            "#95ff6b",
            "#9269ff",
            "#d57a7a",
            /**************/
            "#ffa3b7",
            "#89c9f4",
            "#ffe39c",
            "#96ff9e",
            "#a9a9a9",
            "#f46ed8"
          ],
          hoverBackgroundColor: [



            /**************/
            "#F44336",
            "#3F51B5",
            "#FFFF00",
            "#1B5E20",
            "#9C27B0",
            "#18FFFF",
            "#FF8F00",
            "#795548",
            "#35B79E",
            "#19b735",
            "#ff4081",
            "#ffe77f",
            "#ff953a",
            "#f245ff",
            "#0effb4",
            "#4bff04",
            "#4500ff",
            "#B71C1C",
            /**************/
            "#FF6384",
            "#36A2EB",
            "#FFCE56",
            "#4cff59",
            "#6d6d6d",
            "#eb09bc"
            /*,
             "#FF6384",
             "#36A2EB",
             "#FFCE56",
             "#4cff59",
             "#6d6d6d",
             "#eb09bc",
             "#FF6384",
             "#36A2EB",
             "#FFCE56",
             "#4cff59",
             "#6d6d6d",
             "#eb09bc"*/
          ]
        }]
      },
      options:{
        title: {
          display: true,
          text: this.filter.anho
        },
        legend: {
          display: false
        },
        tooltips: {
          callbacks: {
            footer: function(tooltipItem, data) {
              return " Total: "+data.datasets[0].data[tooltipItem[0].index];
            },
            label: function(tooltipItem, data) {
              return data.labels[tooltipItem.index] +": "+Math.round(((data.datasets[0].data[tooltipItem.index]*100) / this.totalSuscripciones))+"%";
            }.bind(this)
          }
        }
      },
      value: this.totalSuscripciones

    });
  }


  showConcursosPcd(){
    this.pcd = new Chart(this.pcdCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        labels: this.tipo,
        datasets: [{
          data: this.cantidadConcursos,
          backgroundColor: [
            'rgba(255, 206, 86, 0.2)',
            '#cffdd7',
            'rgba(167, 167, 167, 0.25)'
          ],
          hoverBackgroundColor: [
            "#FFCE56",
            "#4cff59",
            "#6d6d6d"
          ]
        }]
      },
      options:{
        title: {
          display: true,
          text: this.filter.anho
        },
        tooltips: {
          callbacks: {
            footer: function(tooltipItem, data) {
              return " Cantidad: "+data.datasets[0].data[tooltipItem[0].index];
            },
            label: function(tooltipItem, data) {
              return data.labels[tooltipItem.index] +": "+Math.round(((data.datasets[0].data[tooltipItem.index]*100) / this.totalConcursos))+"%";
            }.bind(this)
          }
        }
      }

    });
  }

  showTips(e) {



    //this.deleteMarkers();
    //this.map.setClickable(false);
    let popover = this.modalCtrl.create(FilterPage, {data: this.filter});

    popover.present();
    popover.onDidDismiss((popoverData) => {
      if(popoverData != null){
        this.removeData(this.barChart);
        this.removeData(this.doughnutChart);
        this.reloadData(popoverData.categoria, popoverData.institucion, popoverData.vinculo, popoverData.anho);
      }
      this.filter = popoverData;
    });
    /*popover.onDidDismiss(data => {
     if (data == 'undefined' || data == null) {
     //this.map.setClickable(true);
     } else {
     this.deleteMarkers();
     this.loader = this.loadingCtrl.create({
     content: "Cargando..."
     });
     this.loader.present();
     this.departamento = data.ciudad;
     this.idCiudad = data.id;
     this.items = [];
     this.loadData(this.departamento, this.nombre, null, null);

     //this.map.setClickable(true);
     }

     });



     //let popover = this.popoverCtrl.create(FilterPage, {data: this.filter});
     let ev = {
     target : {
     getBoundingClientRect : () => {
     return {
     top: '70',
     left: '90'
     };
     }
     }
     };
     popover.present({ev: ev});

     popover.onDidDismiss((popoverData) => {
     if(popoverData != null){
     this.removeData(this.barChart);
     this.removeData(this.doughnutChart);
     this.reloadData(popoverData.categoria, popoverData.institucion, popoverData.vinculo, popoverData.anho);
     }
     this.filter = popoverData;
     })*/
  };
  removeData(chart) {
    chart.data.labels = [];
    chart.data.datasets.forEach((dataset) => {
      dataset.data = [];
    });
  }
  addData(chart, label, data) {
    chart.data.labels = label;
    chart.data.datasets.forEach((dataset) => {
      dataset.data = data;
    });
    chart.update();
  }
  addDataBar(chart, label, data1, data2) {
    chart.data.labels = label;
    chart.data.datasets[0].data = data1;
    chart.data.datasets[1].data = data2;
    chart.update();
  }

  refresh() {
    this.filter = {};
    this.filter.anho= this.today.getFullYear();
    this.reloadData(null, null, null, 2017)
  }
}
