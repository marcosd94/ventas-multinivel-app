import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { UtilsService } from "./utils-service";
import { UserService } from "./user-service";

/*
  Generated class for the MapaInstitucionesService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ClientesService {

  constructor(public http: Http, private utils: UtilsService, private userService: UserService) {
  }
  loadClientes(ciudad: string, nombre: string, latitud: any, longitud: any) {

    //search.set('filtros','{}');
    /*search.set('ciudad', ciudad);
    search.set('descripcionOee', nombre);
    search.set('latitud', latitud);
    search.set('longitud', longitud);*/

    var param = {};
    var search = new URLSearchParams();
    var fin = JSON.stringify(param);
    search.set("filtros", fin);

    /*return this.http.get('http://datos.sfp.gov.py/api/rest/oee/oficina', { search })*/
    return this.http.get('http://' + this.utils.URL_DESA02 + '/rest/clientes-view/vista', { search })
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({ list: [] });
      });
  }

  loadListClientes(usuarioLogueado: string) {

    var param = { idVendedor: usuarioLogueado };
    var search = new URLSearchParams();
    var fin = JSON.stringify(param);
    search.set("filtros", fin);

    /*return this.http.get('http://datos.sfp.gov.py/api/rest/oee/oficina', { search })*/
    return this.http.get('http://' + this.utils.URL_DESA02 + '/rest/clientes-view/vista', { search })
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({ list: [] });
      });
  }

  loadCiudad(ciudad: string) {

    var param = {};
    var search = new URLSearchParams();
    var fin = JSON.stringify(param);
    search.set("filtros", fin);

    /*return this.http.get('http://datos.sfp.gov.py/api/rest/oee/oficina', { search })*/
    return this.http.get('http://' + this.utils.URL_DESA02 + '/rest/clientes-view/vista', { search })
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({ list: [] });
      });
  }



  getClientes(departamento, nombre, latitud, longitud) {
    return this.loadClientes(departamento, nombre, latitud, longitud);
  }

  getListaClientes(usuarioLogueado) {
    return this.loadListClientes(usuarioLogueado);
  }

  getCiudad(ciudad) {
    return this.loadCiudad(ciudad);
  }

  insertarCliente(param) {
    //search.set('orden', 'ASC');
    return this.http.post('http://'+this.utils.URL_DESA02+'/rest/clientes/insertar', param)
      .map(res => {
        let user = res.json();
        return user;
      })
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of([]);
      });
  }

}
