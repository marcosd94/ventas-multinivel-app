import { Injectable } from '@angular/core';
import { Http, URLSearchParams   } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import {UtilsService} from "./utils-service";

/*
  Generated class for the MapaInstitucionesService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MapaClientesService {

  constructor(public http: Http,  private utils: UtilsService) {
  }
  loadClientes(ciudad : string, nombre: string, latitud: any, longitud: any) {

    //search.set('filtros','{}');
    /*search.set('ciudad', ciudad);
    search.set('descripcionOee', nombre);
    search.set('latitud', latitud);
    search.set('longitud', longitud);*/

    var param = {};
    var search = new URLSearchParams();
    var fin = JSON.stringify(param);
    search.set("filtros", fin);

    /*return this.http.get('http://datos.sfp.gov.py/api/rest/oee/oficina', { search })*/
    return this.http.get('http://'+this.utils.URL_DESA02+'/rest/clientes-view/vista', { search })
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({ list: [] });
      });
  }

  loadCiudad(ciudad : string) {
    var search = new URLSearchParams();
    search.set('ciudad', ciudad);

    return this.http.get('http://datos.sfp.gov.py/api/rest/oee/ciudades', { search })
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({ list: [] });
      });
  }



  getClientes(departamento, nombre, latitud, longitud) {
    return this.loadClientes(departamento, nombre, latitud, longitud);
  }

  getCiudades(ciudad) {
    return this.loadCiudad(ciudad);
  }

}
