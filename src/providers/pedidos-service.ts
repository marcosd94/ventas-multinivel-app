import {Injectable} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import {UtilsService} from "./utils-service";
import {UserService} from "./user-service";
import {AuthService} from "./auth-service";

/*
 Generated class for the ConcursosService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class PedidosService {

  inicios: any = {};
  nombres: any = {};
  pedido : any = null;
  CANTIDAD: number = 10;

  constructor(public http: Http, private utilsService: UtilsService, private userService: UserService, public auth: AuthService) {
  }

  load(tipo: string, nombre: string, reinicio : boolean, orden: string) {
    if (!this.inicios[tipo] || reinicio) {
      this.inicios[tipo] = 0;
    }
    if (!this.nombres[tipo]) {
      this.nombres[tipo] = null;
    }
    if (this.nombres[tipo] != nombre) {
      this.inicios[tipo] = 0;
      this.nombres[tipo] = nombre;
    }
    var search = new URLSearchParams();
    if(reinicio){
      this.inicios[tipo] = 0;
    }
    if (this.nombres[tipo]) {
      search.set('nombre', '' + this.nombres[tipo]);
    }
    search.set('inicio', '' + this.inicios[tipo]);
    search.set('cantidad', '' + this.CANTIDAD);
    search.set('orden', orden);
    this.inicios[tipo] += this.CANTIDAD;
    return this.http.get('http://datos.sfp.gov.py/api/rest/concursos/listByEstado/' + tipo, {search})
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of([]);
      });
  }

  getPedidosPendientes(nombre: string, reinicio : boolean) {


    if (!this.inicios['PEN'] || reinicio) {
      this.inicios['PEN'] = 0;
    }
    /*while(typeof this.userService.user == 'undefined'){

    }*/
    var param = {
      estadoPedido:'PEN',
      codigoUsuario: this.userService.user.codigoUsuario
    };
    var search = new URLSearchParams();
    var fin = JSON.stringify(param);
    search.set("filtros", fin);
    search.set('inicio', '' + this.inicios['PEN']);
    search.set('cantidad', '' + this.CANTIDAD);
    this.inicios['PEN'] += this.CANTIDAD;

    return this.http.get('http://'+this.utilsService.URL_DESA02+'/rest/pedidos-clientes-view/vista', {search})
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of([]);
      });
  }

  getPedidosCerrados(nombre: string, reinicio : boolean) {
    if (!this.inicios['CER'] || reinicio) {
      this.inicios['CER'] = 0;
    }
    var param = {
      estadoPedido:'CER',
      codigoUsuario: this.userService.user.codigoUsuario
    };
    var search = new URLSearchParams();
    var fin = JSON.stringify(param);
    search.set("filtros", fin);
    search.set('inicio', '' + this.inicios['CER']);
    search.set('cantidad', '' + this.CANTIDAD);
    this.inicios['CER'] += this.CANTIDAD;


    return this.http.get('http://'+this.utilsService.URL_DESA02+'/rest/pedidos-clientes-view/vista', {search})
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of([]);
      });
  }
  getPedidosFinalizados(nombre: string, reinicio : boolean) {
    if (!this.inicios['FIN'] || reinicio) {
      this.inicios['FIN'] = 0;
    }
      var param = {
        estadoPedido:'FIN',
        codigoUsuario: this.userService.user.codigoUsuario
      };
      var search = new URLSearchParams();
      var fin = JSON.stringify(param);
      search.set("filtros", fin);
    search.set('inicio', '' + this.inicios['FIN']);
    search.set('cantidad', '' + this.CANTIDAD);
    this.inicios['FIN'] += this.CANTIDAD;


      return this.http.get('http://'+this.utilsService.URL_DESA02+'/rest/pedidos-clientes-view/vista', {search})
        .timeout(10000)
        .map(res => res.json())
        .catch(err => {
          return Observable.of([]);
        });
  }
  insertarPedido(pedidosClientesParam){
    return this.http.post('http://'+this.utilsService.URL_DESA02+'/rest/pedidos-clientes/insertar-pedidos-clientes', pedidosClientesParam)
      .map(res => {
        this.pedido = res.json();
        return this.pedido;
      })
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of(err);
      });
  }
  getPedidosDetalles(idPedido, crear) {

    var param = {};
    if(!crear){
      param = {id:idPedido};
    }
    var search = new URLSearchParams();
    var fin = JSON.stringify(param);
    search.set("filtros", fin);
    let accion = crear? 'administrar' : 'vista';

    return this.http.get('http://'+this.utilsService.URL_DESA02+'/rest/pedidos-detalles-clientes-view/'+accion, {search})
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of([]);
      });
  }

  actualizarPedido(pedidosClientesParam){
    return this.http.post('http://'+this.utilsService.URL_DESA02+'/rest/pedidos-clientes/actualizar-pedidos-clientes', pedidosClientesParam)
      .map(res => {
        this.pedido = res.json();
        return this.pedido;
      })
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of(err);
      });
  }

  cerrarPedido(id){
    return this.http.post('http://'+this.utilsService.URL_DESA02+'/rest/pedidos-clientes/cerrar-pedido/'+id, {})
      .map(res => {
        this.pedido = res.json();
        return this.pedido;
      })
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of(err);
      });
  }
  eliminarPedido(id){
    return this.http.post('http://'+this.utilsService.URL_DESA02+'/rest/pedidos-clientes/eliminar-pedido/'+id, {})
      .map(res => {
        this.pedido = res.json();
        return this.pedido;
      })
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of(err);
      });
  }
}
