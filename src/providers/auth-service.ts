import { Injectable } from '@angular/core';
import { AuthProviders, AngularFireAuth, FirebaseAuthState, AuthMethods } from 'angularfire2';

import { Platform, ToastController} from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';

import * as firebase from "firebase";
import {UserService} from "./user-service";

@Injectable()
export class AuthService {

  private authState: FirebaseAuthState;
  auth:any = null;
  userData:any=null;

  constructor(public auth$: AngularFireAuth, private platform: Platform, private googlePlus: GooglePlus, public toastCtrl: ToastController,  public userService: UserService) {
    this.authState = auth$.getAuth();
    this.auth = auth$;
    auth$.subscribe((state: FirebaseAuthState) => {
      this.authState = state;
      if (this.authState) {
        let correoElectronico = this.authState.google.email;

        this.userService.verificarUsuario(correoElectronico)
          .subscribe((payload) => {
            this.userService.user = payload;
            if(this.userService.user == null  || typeof this.userService.user == 'undefined' ){
              this.userService.user = null;
              //this.showToast("Bienvenido: "+usuario.nombres +' '+usuario.apellidos);
              //this.navCtrl.push(ConcursosPage);
            }
          });
       /* this.userService.obtenerUsuario(idFirebase).subscribe((user) => {
          this.userService.user = user;
        });*/
      }
    });
  }


  get authenticated(): boolean {
    return this.authState !== null;
  }

  signInWithGoogle(): firebase.Promise<any> {
    if (this.platform.is('cordova')) {
      return this.googlePlus.login({
        'webClientId': '488619158537-h5i687njddvgirjg10ginarcc52qj5f9.apps.googleusercontent.com',
        'offline': true
      }).then(res => {
        const credential = firebase.auth.GoogleAuthProvider.credential(res.idToken);
        return firebase.auth().signInWithCredential(credential);
      })
        .catch((e) => this.onError(e));
    } else {
      return this.auth$.login({
        provider: AuthProviders.Google,
        method: AuthMethods.Popup
      });
    }
  }

  onError(error): void {
    let errorMessage = error.message;
    this.showToast(errorMessage);
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }


  signOut(): void {
    this.auth$.logout();
    this.userService.user = null;
  }

  displayName(): string {
    if (this.authState != null) {
      if (this.authState.facebook) {
        return this.authState.facebook.displayName;
      } else if (this.authState.google) {
        return this.authState.google.displayName;
      } else {
        return this.authState.auth.email;
      }
    } else {
      return '';
    }
  }

  email(): string {
    if (this.authState != null) {
      if (this.authState.facebook) {
        return this.authState.facebook.email;
      } else if (this.authState.google) {
        return this.authState.google.email;
      }
    } else {
      return '';
    }
  }

  displayAvatar(): string {
    if (this.authState != null) {
      if (this.authState.facebook) {
        return this.authState.facebook.photoURL;
      } else if (this.authState.google) {
        return this.authState.google.photoURL;
      }else{
        return "./assets/img/avatar.png";
      }
    } else {
      return '';
    }
  }

}
