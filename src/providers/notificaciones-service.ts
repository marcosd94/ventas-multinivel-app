import {Injectable} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

/*
 Generated class for the ConcursosService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class NotificacionesService {

  constructor(public http: Http) {
  }

  getNotificaciones(idFirebase: string) {
    var search = new URLSearchParams();
    search.set('idFirebase', idFirebase);
    return this.http.get('http://datos.sfp.gov.py:8081/concursa-py/rest/notificaciones/listar-notificaciones', {search})
      .map(res => res.json())
      .catch(err => {
        return Observable.of([]);
      });
  }
  marcarNotificacionLeida(idNotificacion:number) {
    //search.set('orden', 'ASC');
    return this.http.post('http://datos.sfp.gov.py:8081/concursa-py/rest/notificaciones/marcar-notificacion-leida?idNotificaciones='+idNotificacion, {})
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of([]);
      });
  }


}
