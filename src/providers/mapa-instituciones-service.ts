import { Injectable } from '@angular/core';
import { Http, URLSearchParams   } from '@angular/http';import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

/*
  Generated class for the MapaInstitucionesService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MapaInstitucionesService {

  constructor(public http: Http) {
  }
  loadOficina(ciudad : string, nombre: string, latitud: any, longitud: any) {
    var search = new URLSearchParams();
    search.set('ciudad', ciudad);
    search.set('descripcionOee', nombre);
    search.set('latitud', latitud);
    search.set('longitud', longitud);

    return this.http.get('http://datos.sfp.gov.py/api/rest/oee/oficina', { search })
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({ list: [] });
      });
  }

  loadCiudad(ciudad : string) {
    var search = new URLSearchParams();
    search.set('ciudad', ciudad);

    return this.http.get('http://datos.sfp.gov.py/api/rest/oee/ciudades', { search })
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({ list: [] });
      });
  }



  getEnInstituciones(departamento, nombre, latitud, longitud) {
    return this.loadOficina(departamento, nombre, latitud, longitud);
  }

  getCiudades(ciudad) {
    return this.loadCiudad(ciudad);
  }

}
