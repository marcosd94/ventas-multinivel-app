import {Injectable} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

/*
 Generated class for the ConcursosService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class ConcursosService {

  inicios: any = {};
  nombres: any = {};

  CANTIDAD: number = 10;

  constructor(public http: Http) {
  }

  load(tipo: string, nombre: string, reinicio : boolean, orden: string) {
    if (!this.inicios[tipo] || reinicio) {
      this.inicios[tipo] = 0;
    }
    if (!this.nombres[tipo]) {
      this.nombres[tipo] = null;
    }
    if (this.nombres[tipo] != nombre) {
      this.inicios[tipo] = 0;
      this.nombres[tipo] = nombre;
    }
    var search = new URLSearchParams();
    if(reinicio){
      this.inicios[tipo] = 0;
    }
    if (this.nombres[tipo]) {
      search.set('nombre', '' + this.nombres[tipo]);
    }
    search.set('inicio', '' + this.inicios[tipo]);
    search.set('cantidad', '' + this.CANTIDAD);
    search.set('orden', orden);
    this.inicios[tipo] += this.CANTIDAD;
    return this.http.get('http://datos.sfp.gov.py/api/rest/concursos/listByEstado/' + tipo, {search})
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of([]);
      });
  }

  loadDetail(tipo: string, identificadorConcursoPuesto: number) {
    return this.http.get('http://datos.sfp.gov.py/api/rest/concursos/' + tipo + '/' + identificadorConcursoPuesto)
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({});
      });
  }

  loadInstitucion(tipo: string, institucion: string) {
    var search = new URLSearchParams();
    search.set('institucion', institucion);
    search.set('inicio', '0');
    search.set('cantidad', '100');
    search.set('orden', 'ASC');
    return this.http.get('http://datos.sfp.gov.py/api/rest/concursos/listByEstado/' + tipo, {search})
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({});
      });
  }

  getEnPostulacion(nombre: string, reinicio : boolean) {
    return this.load('POSTULACIÓN', nombre, reinicio, 'DESC');
  }

  getEnEvaluacion(nombre: string, reinicio : boolean) {
    return this.load('EVALUACIÓN', nombre, reinicio, 'DESC');
  }

  getFinalizados(nombre: string, reinicio : boolean) {
    return this.load('FINALIZADOS', nombre, reinicio, 'DESC');
  }

  getDetalleEnPostulacion(identificadorConcursoPuesto: number) {
    return this.loadDetail('POSTULACION', identificadorConcursoPuesto);
  }

  getDetalleEnEvaluacion(identificadorConcursoPuesto: number) {
    return this.loadDetail('EVALUACION', identificadorConcursoPuesto);
  }

  getDetalleFinalizado(identificadorConcursoPuesto: number) {
    return this.loadDetail('FINALIZADOS', identificadorConcursoPuesto);
  }

  getEnPostulacionInstitucion(institucion: string) {
    return this.loadInstitucion('POSTULACIÓN', institucion);
  }

}
