import {Injectable} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import {UtilsService} from "./utils-service";

/*
 Generated class for the ConcursosService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class ProductosService {

  constructor(public http: Http, private utilsService : UtilsService) {
  }
  getProductos() {
    var param = {};
    var search = new URLSearchParams();
    var fin = JSON.stringify(param);
    search.set("filtros", fin);

    return this.http.get('http://'+this.utilsService.URL_DESA02+'/rest/producto-view/vista', {search})
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of([]);
      });
  }

}
