import {Injectable} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import {UtilsService} from "./utils-service";

/*
 Generated class for the UserService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class UserService {

  public user: any;

  constructor(public http: Http, private utilsService : UtilsService) {
  }

  crearUsuario(idFirebase: string, email: string, nombres: string) {
    return this.http.post('http://datos.sfp.gov.py:8081/concursa-py/rest/usuarios/agregar', {
      "idFirebase": idFirebase,
      "nombres": nombres,
      "correoElectronico": email,
    })
      .map(res => {
        this.user = res.json();
        return this.user;
      })
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of([]);
      });
  }

  obtenerUsuario(idFirebase: string) {
    var search = new URLSearchParams();
    search.set("idFirebase", idFirebase);
    return this.http.get('http://datos.sfp.gov.py:8081/concursa-py/rest/usuarios/obtener', {search})
      .map(res => {
        this.user = res.json();
        return this.user;
      })
      .catch(err => {
        return Observable.of({});
      });
  }

  verificarUsuario(emailUsuario: string) {
    var param = {"email":emailUsuario}
    var search = new URLSearchParams();
    var fin = JSON.stringify(param);
    search.set("filtros", fin);
    return this.http.get('http://'+this.utilsService.URL_DESA02+'/rest/usuario-view/vista', {search})
      .map(res => {
        this.user = res.json();
        if(this.user.lista.length > 0){
          this.user = this.user.lista[0];
          return this.user;
        }else{
          return null;
        }
      })
      .catch(err => {
        return Observable.of({});
      });
  }
  modificarUsuario(idFirebase: string, idUsuario: string, email: string, nombres: string, apellidos: string,
                   estadoCivil: string, sexo: string, documento: string, fechaNacimiento: string, fechaCreacion: string, telefono: string ) {
    //search.set('orden', 'ASC');
    return this.http.post('http://datos.sfp.gov.py:8081/concursa-py/rest/usuarios/modificar', {
      "idFirebase": idFirebase,
      "correoElectronico": email,
      "nombres": nombres,
      "idUsuario": idUsuario,
      "estadoCivil": estadoCivil,
      "apellidos": apellidos,
      "sexo": sexo,
      "documento": documento,
      "fechaNacimiento": fechaNacimiento,
      "fechaCreacion": fechaCreacion,
      "telefono":telefono,
    })
      .map(res => {
        this.user = res.json();
        return this.user;
      })
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of([]);
      });
  }

  guardarRegistrationId(idUsuario: string, token: string) {
    return this.http.post('http://datos.sfp.gov.py:8081/concursa-py/rest/token/insertar-registration-token?idUsuario='
      + idUsuario + "&token=" + token, {})
      .map(res => res.json())
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of([]);
      });
  }

}
