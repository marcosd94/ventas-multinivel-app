import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

/*
 Generated class for the ConcursosService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class ImportanteService {

  constructor(public http: Http) {
  }


  getTipsImportantes() {
    return this.http.get('http://datos.sfp.gov.py/api/rest/concursos/tips-concursos')
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        return Observable.of({});
      });
  }


}
