import { Injectable } from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";

/*
  Generated class for the CategoriasProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FilterProvider {

  constructor(public http: Http) {
  }

  getInstituciones(institucion:any) {
    var search = new URLSearchParams();
    search.set('descripcionOee', institucion);
    return this.http.get('http://datos.sfp.gov.py/api/rest/oee', { search })
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of([]);
      });
  }

  getCategorias(categoria:any) {
    var search = new URLSearchParams();
    search.set('categoria', categoria);
    search.set('orden', 'ASC');
    return this.http.get('http://datos.sfp.gov.py/api/rest/concursos/categorias', { search })
      .timeout(10000)
      .map(res => res.json())
      .catch(err => {
        console.log("Ocurrio un error " + JSON.stringify(err));
        return Observable.of([]);
      });
  }
}
