# VENTAS MULTINIVEL
## Installation

1. Download Proyect<br>
```
git clone url_del_proyecto.git
```
<br>
2. Install NOdejs<br>
``` https://nodejs.org/es/download/
```
<br>
3. Install IONIC 2<br>
```
npm install -g cordova ionic@2.2.2
```
<br>
4. Acceder al directorio del Proyecto<br>
```
cd ventas-multinivel-app
```
<br>
5. Install npm<br>
```
npm install
```
<br>
6. Agregar plataforma android al Proyecto<br>
```
ionic platform add android
```
<br>
7. Crear clave para la App Android<br>
```
cd platforms/android
keytool -genkey -v -keystore ConcursaPy.keystore -alias ConcursaPy -keyalg RSA -keysize
2048 -validity 10000
```

8. Copiar el archivo release-signing.properties<br>
```
cp release-signing.properties platforms/android
*Modificar según la contraseña para el KeyStore de Android
```

9. Modificar el archivo que se encuentra en platforms/android/build.gradle<br>
En la propiedad dependencies agregar:<br>

```
dependencies {
classpath 'com.android.tools.build:gradle:2.2.3'
classpath 'com.google.gms:google-services:3.1.0'
}

```
10 . En el final del archivo platforms/android/build.gradle agregar la línea:

```
apply plugin: 'com.google.gms.google-services'

```

11 . Modificar el archivo que se encuentra en platforms/android/project.propiertes <br>
Modificar solo el número de las versiones por las siguientes.<br>

```
cordova.system.library.1=com.facebook.android:facebook-android-sdk:4.+
cordova.system.library.2=com.google.firebase:firebase-core:9.8.0
cordova.system.library.3=com.google.firebase:firebase-messaging:9.8.0
cordova.system.library.4=com.google.android.gms:play-services-analytics:9.8.0
cordova.system.library.5=com.google.android.gms:play-services-maps:9.8.0
cordova.system.library.6=com.google.android.gms:play-services-location:9.8.0
cordova.system.library.7=com.google.android.gms:play-services-auth:9.8.0
cordova.system.library.8=com.google.android.gms:play-services-identity:9.8.0
cordova.system.library.9=com.android.support:support-v4:25.+
cordova.system.library.10=com.android.support:appcompat-v7:25.+

```
<br>
12 . Eliminar la siguiente línea en el archivo platforms/android/project.propiertes <br>

```
cordova.system.include.1=cordova-plugin-fcm/concursaparaguay-FCMPlugin.gradle

```
<br>
13 . Lanzar la aplicación android:<br>
```
ionic run android
```


```
<br>
14 . Construir la aplicación android:<br>
```
ionic build android --prod --release
```
